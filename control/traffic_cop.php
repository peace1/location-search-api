<?php
	require('../connection/db_connection.php');
	require('../util/request.php');

	$str_json = $_GET['json'];
	$json_req = json_decode($str_json);

	$requestType = $json_req->requestType;
	$db = new DB_CONNECT();
	switch($requestType){
		case Request::LOGIN:
			$email = $json_req->email;
			$password = $json_req->password;
			DataUtil::login($email,$password);
			break;
		case Request::REGISTER:
			DataUtil::register($json_req);
			break;
		case Request::GET_ACCOMODATION_PROXIMITY:
			DataUtil::getAccomodationByProximity($json_req);
			break;
		case Request::GET_FLAT_BY_NAME:
			DataUtil::findAccomodationByName($json_req);
			break;
		case Request::GET_FLAT_BY_OWNER:
			DataUtil::findAccomodationByOwner($json_req);
			break;
		case Request::GET_FLAT_BY_PRICE:
			DataUtil::findAccomodationByPrice($json_req);
			break;
		default:
			//Convert to json
	}

class DataUtil{

	const KILOMETRES =1;
	const MILES = 2;
	const PARM_KM = 6371;
	const PARM_MILES = 3959;
	
	const ONE = 1;
	const TWO = 2;
	const THREE = 3;
	const FOUR = 4;
	const FIVE = 5;
	const SIX = 6;
	const SEVEN = 7;
	const EIGTH = 8;
	const NINE = 9;

	public static function login($email, $password){
		$results = mysql_query("select * from user where email = '$email' and password = '$password'");
		if(mysql_num_rows($results) > 0){
			while ($r = mysql_fetch_assoc($results)) {
				$result["user"] = $r;
			
				//do an lastseen update
				
				if($result != null){
					$isAdded = addGCMDevice($json_req,$userID);	
				}
				
			}
			
		}else{
			$result["statusCode"] = 2;
			$result["message"] = "Accomodation name not found, please re-check spelling";
			$result["user"] = null;
			
		}
		echo json_encode($result);
	}
	
	public static function getUser($userID){
		$result = array();
		$results = mysql_query("select * from user where userID = '$userID'");
		if(mysql_num_rows($results) > 0){
			while ($r = mysql_fetch_assoc($results)) {			
				$result = $r;
				//do an lastseen update				
			}			
			
		}else{
			$result = null;
			
		}
		return $result;
	}
	
	public static function register($json_req){
		$resp = array();
		$name = $json_req->name;
		$email = $json_req->email;
		$dob = $json_req->dob;
		$userType = $json_req->userType;
		$gender = $json_req->gender;
		$cellNo = $json_req->cellNo;
		$lastSeen =  date('Y-m-d H:i:s');
		$passWord = $json_req->password;
		$photoUrl = $json_req->photoUrl;
		$result = mysql_query("insert into user(name, email, dob, userType, gender, cellNo, lastSeen, password, photoUrl) 
		values('$name','$email', '$dob', '$userType', '$gender', '$cellNo', '$lastSeen', '$password', '$photoUrl')");
		
		echo mysql_error();
		
		if($result){
			$results = mysql_query("select * from user where email = '$email' and password = '$password'");
			
			if (mysql_num_rows($results) > 0) {
				$row = mysql_fetch_array($results);
				$userID = $row["userID"];
				$isAdded = addGCMDevice($json_req,$userID);	
				if($isAdded){
					sendEmail($name,$email);
					$resp["statusCode"] = 0;
					$resp["message"] = "Registration successfull, please check your email for confirmation";
					$resp["user"] = null;
				}else{
					$resp["statusCode"] = 2;
					$resp["message"] = "Registration not successfull";
					$resp["user"] = null;
				}
			}else{
				$resp["statusCode"] = 2;
				$resp["message"] = "Registration not successfull";
				$resp["user"] = null;
			}
			
			
		}  else{
			$resp["statusCode"] = 2;
			$resp["message"] = "Registration not successfull";
			$resp["user"] = null;
		}   
		echo json_encode($resp);		
	}
	
	
	public static function getAccomodationByProximity($json_req){
		$resp = array();
		$latitude = $json_req->latitude;
		$longitude = $json_req->longitude;
		$radius = $json_req->radius;
		$countryMeasure = 0;
		$type = $json_req->type;
		switch($type){
			case DataUtil::KILOMETRES:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			case DataUtil::MILES:
				$countryMeasure = DataUtil::PARM_MILES;
				break;
			case 0:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			default:
			break;
		}		
		
		$query = sprintf("SELECT address.addressID, address.AddressName, address.streetName, address.streetAddress, address.latitude, address.longitude, address.suburb, 
address.country, accomodation.accomodationID,accomodation.name, accomodation.userID,accomodation.datePosted,accomodation.status , accomodation.price ,
( '%s' * acos( cos( radians('%s') ) * cos( radians( address.latitude ) ) * cos( radians( address.longitude ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( address.latitude ) ) ) ) AS distance FROM address 
INNER JOIN accomodation
ON address.addressID = accomodation.addressID
HAVING distance < '%s' ORDER BY distance",
		mysql_real_escape_string($countryMeasure),
		mysql_real_escape_string($latitude),
		mysql_real_escape_string($longitude),
		mysql_real_escape_string($latitude),
		mysql_real_escape_string($radius));
		
		$result = mysql_query($query);
		
		if (!$result) {
			$resp["statusCode"] = 2;
			$resp["message"] = "Something went wrong";
			echo json_encode($resp);
			die("Invalid query: " . mysql_error());
		}
		$resp["accomodation"] = array();
		while($row = mysql_fetch_assoc($result)){
			
			
			$accomodation = DataUtil::getPhotos($row['accomodationID']);	
			$accomodation['accomodationID'] = $row['accomodationID'];
			$accomodation['name'] = $row['name'];
			$accomodation['datePosted'] = $row['datePosted'];
			$accomodation['status'] = $row['status'];
			$accomodation["price"] = $row["price"];	
			$accomodation['owner'] = DataUtil::getUser($row['userID']);
				
		
			$address['addressID'] = $row['addressID'];
			$address['AddressName'] = $row['AddressName'];
			$address['streetName'] = $row['streetName'];
			$address['streetAddress'] = $row['streetAddress'];
			$address['latitude'] = $row['latitude'];
			$address['longitude'] = $row['longitude'];
			//$address['suburb'] = $row['suburb'];
			$address['country'] = $row['country'];
			$address['distance'] = $row['distance'];
			$accomodation['address'] = $address;
			
			array_push($resp['accomodation'], $accomodation);			
		}
		$resp["statusCode"] = 0;
		echo json_encode($resp);
	}
	
	public static function getPhotos($accomodationID){
		$photos['photos'] = array();
		
		$results = mysql_query("SELECT * FROM photo where accomodationID = '$accomodationID'");
		
		if(mysql_num_rows($results) > 0){
			while ($row = mysql_fetch_assoc($results)) {
				$photo['photoID'] = $row['photoID'];
				$photo['photoUrl'] = $row['url'];
				
				array_push($photos["photos"], $photo);
			}
		
		//	echo json_encode($photos);
			return $photos;
		}else{
			return null;
		}	
				
	}	
	
	public static function findAccomodationByName($json_req){			
		$response = array();
		$latitude = $json_req->latitude;
		$longitude = $json_req->longitude;
		$radius = $json_req->radius;
		$countryMeasure = 0;
		$name = $json_req->name;	
		$type = $json_req->type;
		switch($type){
			case DataUtil::KILOMETRES:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			case DataUtil::MILES:
				$countryMeasure = DataUtil::PARM_MILES;
				break;
			case 0:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			default:
			break;
		}	
		
		$query = "SELECT address.addressID, address.AddressName, address.streetName, address.streetAddress, address.latitude, address.longitude, address.suburb, 
address.country, accomodation.accomodationID,accomodation.name, accomodation.userID,accomodation.datePosted,accomodation.status , accomodation.price ,
( $countryMeasure * acos( cos( radians($latitude) ) * cos( radians( address.latitude ) ) * cos( radians( address.longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( address.latitude ) ) ) ) AS distance FROM address 
INNER JOIN accomodation
ON address.addressID = accomodation.addressID
WHERE accomodation.name like '%$name%'
HAVING distance < $radius ORDER BY distance";

		$results = mysql_query($query);
		
		if (mysql_num_rows($results) > 0) {
			//$response["address"] = array
			$response["accomodation"] = array();
			while ($row = mysql_fetch_assoc($results)) {	
				
				//$accomodation = $row;
				$accomodation = DataUtil::getPhotos($row["accomodationID"]);
				$accomodation["accomodationID"] = $row["accomodationID"];
				$accomodation["name"] = $row["name"];
				$accomodation["datePosted"] = $row["datePosted"];
				$accomodation["status"] = $row["status"];
				$accomodation["price"] = $row["price"];	
				$accomodation["owner"] = DataUtil::getUser($row["userID"]);						
				
				$address['addressID'] = $row['addressID'];
				$address['AddressName'] = $row['AddressName'];
				$address['streetName'] = $row['streetName'];
				$address['streetAddress'] = $row['streetAddress'];
				$address['latitude'] = $row['latitude'];
				$address['longitude'] = $row['longitude'];
				//$address['suburb'] = $row['suburb'];
				$address['country'] = $row['country'];
				$address['distance'] = $row['distance'];
				$accomodation['address'] = $address;
				
				array_push($response["accomodation"], $accomodation);
			
				
			}
			//echo json_encode($response);
			
		}else{
			$response["statusCode"] = 2;
			$response["message"] = "Registration not successfull";
			$response["accomodation"] = null;
		}
		$response["status"] = 0;
		echo json_encode($response);
	}
	
	public static function findAccomodationByOwner($json_req){
		
		$resp = array();
		$latitude = $json_req->latitude;
		$longitude = $json_req->longitude;
		$radius = $json_req->radius;
		$countryMeasure = 0;
		$name = $json_req->name;	
		$type = $json_req->type;
		switch($type){
			case DataUtil::KILOMETRES:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			case DataUtil::MILES:
				$countryMeasure = DataUtil::PARM_MILES;
				break;
			case 0:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			default:
			break;
		}	
		
		$query = "SELECT address.addressID, address.AddressName, address.streetName, address.streetAddress, address.latitude, address.longitude, address.suburb, 
address.country, accomodation.accomodationID,accomodation.name, accomodation.userID,accomodation.datePosted,accomodation.status , accomodation.price ,
( $countryMeasure * acos( cos( radians($latitude) ) * cos( radians( address.latitude ) ) * cos( radians( address.longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( address.latitude ) ) ) ) AS distance FROM address 
INNER JOIN accomodation
ON address.addressID = accomodation.addressID
where  accomodation.userID in (SELECT user.userID from user where user.name LIKE '%$name%')
HAVING distance < $radius ORDER BY distance";

		$results = mysql_query($query);
		
		if (mysql_num_rows($results) > 0) {
			$resp["accomodation"] = array();
			while ($row = mysql_fetch_assoc($results)) {			
				$accomodation = DataUtil::getPhotos($row["accomodationID"]);
				$accomodation["accomodationID"] = $row["accomodationID"];
				$accomodation["name"] = $row["name"];
				$accomodation["datePosted"] = $row["datePosted"];
				$accomodation["status"] = $row["status"];
				$accomodation["price"] = $row["price"];	
				$accomodation["owner"] = DataUtil::getUser($row["userID"]);						
				
				$address['addressID'] = $row['addressID'];
				$address['AddressName'] = $row['AddressName'];
				$address['streetName'] = $row['streetName'];
				$address['streetAddress'] = $row['streetAddress'];
				$address['latitude'] = $row['latitude'];
				$address['longitude'] = $row['longitude'];
				//$address['suburb'] = $row['suburb'];
				$address['country'] = $row['country'];
				$address['distance'] = $row['distance'];
				$accomodation['address'] = $address;
			
				array_push($resp['accomodation'], $accomodation);
			}
			
		}else{
			$resp["statusCode"] = 2;
			$resp["message"] = "Registration not successfull";
			$resp["accomodation"] = null;
		}
		echo json_encode($resp);
	}
	
	public static function findAccomodationByPrice($json_req){
		 
		$PriceAmt = array(0,1000,2000,3000,4000,5000);

		
		
		
		$resp = array();
		$latitude = $json_req->latitude;
		$longitude = $json_req->longitude;
		$radius = $json_req->radius;
		$countryMeasure = 0;
		$price = $json_req->price;	
		$type = $json_req->type;
		
		$moreOrless =$json_req->moreOrless;
		switch($type){
			case DataUtil::KILOMETRES:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			case DataUtil::MILES:
				$countryMeasure = DataUtil::PARM_MILES;
				break;
			case 0:
				$countryMeasure = DataUtil::PARM_KM;
				break;
			default:
			break;
		}	
		$tempPrice1 = 0;
		$tempPrice2 = 0;
		switch($price){
			case DataUtil::ONE:
				$tempPrice1 = $PriceAmt[0];
				$tempPrice2 = $PriceAmt[1];
				break;
			case DataUtil::TWO:
				$tempPrice1 = $PriceAmt[1];
				$tempPrice2 = $PriceAmt[2];
				break;
			case DataUtil::THREE:
				$tempPrice1 = $PriceAmt[2];
				$tempPrice2 = $PriceAmt[3];
				break;
			case DataUtil::FOUR:
				$tempPrice1 = $PriceAmt[3];
				$tempPrice2 = $PriceAmt[4];
				break;
			case DataUtil::FIVE:
				$tempPrice1 = $PriceAmt[4];
				$tempPrice2 = $PriceAmt[5];
				break;
			case DataUtil::SIX:
				$tempPrice1 = $PriceAmt[0];
				$tempPrice2 = $PriceAmt[2];
				break;
			case DataUtil::SEVEN:
				$tempPrice1 = $PriceAmt[0];
				$tempPrice2 = $PriceAmt[3];
				break;
			case DataUtil::EIGTH:
				$tempPrice1 = $PriceAmt[0];
				$tempPrice2 = $PriceAmt[4];
				break;
			case DataUtil::NINE:
				$tempPrice1 = $PriceAmt[0];
				$tempPrice2 = $PriceAmt[5];
				break;
			default;
	
			break;
		}
			
		$query = "";
		if($moreOrless < 1){
			$query = "SELECT address.addressID, address.AddressName, address.streetName, address.streetAddress, address.latitude, address.longitude, address.suburb, 
			address.country, accomodation.accomodationID,accomodation.name, accomodation.userID,accomodation.datePosted,accomodation.status , accomodation.price ,
			( $countryMeasure * acos( cos( radians($latitude) ) * cos( radians( address.latitude ) ) * cos( radians( address.longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( address.latitude ) ) ) ) AS distance FROM address 
			INNER JOIN accomodation
			ON address.addressID = accomodation.addressID
			where accomodation.price between $tempPrice1 and $tempPrice2
			HAVING distance < $radius ORDER BY distance";
			
		}else{
			$query = "SELECT address.addressID, address.AddressName, address.streetName, address.streetAddress, address.latitude, address.longitude, address.suburb, 
			address.country, accomodation.accomodationID,accomodation.name, accomodation.userID,accomodation.datePosted,accomodation.status , accomodation.price ,
			( $countryMeasure * acos( cos( radians($latitude) ) * cos( radians( address.latitude ) ) * cos( radians( address.longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( address.latitude ) ) ) ) AS distance FROM address 
			INNER JOIN accomodation
			ON address.addressID = accomodation.addressID
			where  accomodation.price >= $price
			HAVING distance < $radius ORDER BY distance";
			
		}
		

		$results = mysql_query($query);
		
		if (mysql_num_rows($results) > 0) {
			$resp["accomodation"] = array();
			while ($row = mysql_fetch_assoc($results)) {		
					
				$accomodation = DataUtil::getPhotos($row["accomodationID"]);
				$accomodation["accomodationID"] = $row["accomodationID"];
				$accomodation["name"] = $row["name"];
				$accomodation["datePosted"] = $row["datePosted"];
				$accomodation["status"] = $row["status"];
				$accomodation["price"] = $row["price"];				
				$accomodation["owner"] = DataUtil::getUser($row["userID"]);						
				
				$address['addressID'] = $row['addressID'];
				$address['AddressName'] = $row['AddressName'];
				$address['streetName'] = $row['streetName'];
				$address['streetAddress'] = $row['streetAddress'];
				$address['latitude'] = $row['latitude'];
				$address['longitude'] = $row['longitude'];
				//$address['suburb'] = $row['suburb'];
				$address['country'] = $row['country'];
				$address['distance'] = $row['distance'];
				$accomodation['address'] = $address;
			
				array_push($resp['accomodation'], $accomodation);
			}
			
		}else{
			$resp["statusCode"] = 2;
			$resp["message"] = "Registration not successfull";
			$resp["accomodation"] = null;
		}
		echo json_encode($resp);
	}
	
	public static function getAddressByID($addressID){
		$results = mysql_query("SELECT * FROM address WHERE addressID = '$addressID'");
		$address = array();
		if (mysql_num_rows($results) > 0) {
			while ($row = mysql_fetch_assoc($results)) {
			
				$address['addressID'] = $row['addressID'];
				//$address['AddressName'] = $row['AddressName'];
				$address['streetName'] = $row['streetName'];
				$address['streetAddress'] = $row['streetAddress'];
				$address['latitude'] = $row['latitude'];
				$address['longitude'] = $row['longitude'];
				//$address['suburb'] = $row['suburb'];
				$address['country'] = $row['country'];
					
			}
			
			//echo json_encode($addresses);
						
		}else{
			//echo json_encode($addresses);
			$address["address"] = null;
		}
		return $address;	
	}
	
	public static function addGCMDevice($json_req,$userID){
		$gcm = $json_req->gcmDevice;
		$gcmDevicesID = $gcm->gcmDevicesID;
		$registrationID = $gcm->registrationID;
		$product = $gcm->product;
		$model = $gcm->model;
		$serialNumber = $gcm->serialNumber;
		$androidVersion = $gcm->androidVersion;
		$manufacturer = $gcm->manufacturer;
		$dateregistered = date('Y-m-d H:i:s');
		
		
		$result = mysql_query("insert into gcmDevices(registrationID, product, userID, model, serialNumber, androidVersion, manufacturer, dateregistered) 
		values('$registrationID', '$product', '$userID', '$model', '$serialNumber', '$androidVersion', '$manufacturer', '$dateregistered')");
		echo mysql_error();
		
		if($result){
			return true;
		}else{
			return false;
		}
	}
	
	public static function loadAllData($json_req){
		
	}
	
	public static function sendEmail($name, $email){
		
		//to do the confirmation email message
		 $RegMessage='
				  <html>
				  <body style="background: orange; color: grey;">
				<div style="margin-left: 20px;">
				  <img src="http://app.chowpos.com/chowpos/images/logo.png" align="center" style="height: 250px; width: 300px;"/><br />
				   <h2>ChowPos - What Are You Craving?</h2>
				  <b>Hi '.$name.'</b></br /><br />
				  Please confirm your email address here
				  <br /><br />
	13 Balfour Street<br />
	Nellmapius<br />
	Pretoria<br />
	0122<br />
	&#169; The Notice Board 2014 property of Codetribe (Pty) Ltd
		</div>          
				  </body>
				  </html>
				  ';
		 $headers = "From: " . strip_tags('faf@gmail.com') . "\r\n";
				   $headers .= "Reply-To: ". strip_tags('faf@gmail.com') . "\r\n";
				   $headers .= "MIME-Version: 1.0\r\n";
				   $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	   
				   $mail = mail($email, "GKElectronics Forgotten Password", $RegMessage, $headers);
	}
}
?>